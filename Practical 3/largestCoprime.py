def gcd (a, b):
    if a == 0 or b == 0:
        return 0

    # base case
    if a == b:
        return a

    # a is greater
    if a > b:
        return gcd(a - b, b)

    return gcd(a, b - a)

# function to find largest
# coprime divisor
def coprime(x, y):
    while gcd(x, y) != 1:
        x = x / gcd(x, y)
    return int(x)


x=int(input())
y=int(input())
print(coprime(x, y))
