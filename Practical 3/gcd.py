def gcd(a, b):
  
    # Everything divides 0
    if (a == 0):
        return b
    if (b == 0):
        return a
  
    # base case
    if (a == b):
        return a
  
    # a is greater
    if (a > b):
        return gcd(a-b, b)
    return gcd(a, b-a)
  
# Driver program to test above function
n=2
x=int(input(""))
y=int(input(""))
for i in range(1,n-1):
    x=int(input(x))
    y=int(input(y))
print(gcd(x,y))