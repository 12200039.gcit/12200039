import math
def closestDivisors(num):
    for root in reversed(range(int(math.sqrt(num + 2)) + 1)):
      for cand in [num + 1, num + 2]:
        if cand % root == 0:
          return [root, cand // root]
print(closestDivisors(999))  