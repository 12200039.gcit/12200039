import math

def primeSieve(n):

	primes = [1 for i in range(n+1)]
	p = 2

	while(p <= math.sqrt(n)):
		if primes[p]:
			for i in range(p*p, n+1, p):
				primes[i] = 0
		p+=1

	return primes


def countPrime(m):
    count = 0
    primeNumber = primeSieve(m)

    for i in range(2, len(primeNumber)):
        if (primeNumber[i] == 1):
            count = count + 1
    return count

if __name__ == '__main__':
	print(countPrime(10))