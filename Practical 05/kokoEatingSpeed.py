import math

def minEatingSpeed(List,h):
        l, r = 1, max(List)
        res = r

        while l <= r:
            k = (l + r) // 2
            hours = 0
            for p in List:
                hours += math.ceil(p / k)

            if hours <= h:
                res = min(res, k)
                r = k - 1
            else:
                l = k + 1
        return res

n = [3,6,7,11]
h=8
print(minEatingSpeed(n,h))