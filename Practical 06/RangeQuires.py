# binary tree having root index 1
def build(s, e, node):
    # base case
    if s == e:
        stree[node] = arr[s-1]
        return arr[s-1]

    m = (s+e)//2
    # build left (s, m)
    left = build(s, m, 2*node)
    # build right (m+1, e)
    right = build(m+1, e, 2*node+1)

    # move up from leaf and fill parent nodes/current node
    stree[node] = left + right
    return stree[node]

# find range sum
def sum(s, e, l, r, node):
    # no overlapp
    if e < l or s > r:
        return 0

    # complete overlap
    if l <= s and r >= e:
        return stree[node] 

    # partial overlap
    mid = (s+e)//2
    q1 = sum(s, mid, l, r, 2*node)
    q2 = sum(mid+1, e, l, r, 2*node+1)

    return q1+q2

def update(s, e, pos, val, node):
    # base case   
    if s == e and s == pos:
        stree[node] = val
        return
    
    mid = (s+e)//2
    if pos <= mid:
        update(s, mid, pos, val, 2*node) # left
    else:
        update(mid+1, e, pos, val, 2*node+1) # right

    # update ancestors
    stree[node] = stree[2*node]+stree[2*node+1]

def buildTree():
    return build(1, n, 1) #(start, end, current node), root index = 1, 

def querySum(l, r):
    return sum(1, n, l, r, 1) # (start, end, left, right, current node)

def queryUpdate(index, val):
    return update(1, n, index, val, 1) # (start, end, position, value, currentnode)

if __name__ == '__main__':
    arr = [3, 2, 4, 5, 1, 1, 5, 3]
    n = len(arr)
    stree = [0]*(4*n)
    buildTree()
    # print(stree)
    print(querySum(1,4)) # 14
    print(querySum(5,6)) # 2
    queryUpdate(2,4)
    # print(stree)
    print(querySum(1,4)) # 11