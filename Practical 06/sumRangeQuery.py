class NumArray:

    def __init__(self, nums):
        self.cum=[0] + list(accumulate(nums))
        

    def sumRange(self, left, right):
        return self.cum[right + 1] - self.cum[left]   

    
    


# Your NumArray object will be instantiated and called as such:
# obj = NumArray(nums)
# param_1 = obj.sumRange(left,right)


# class NumArray(object):
#     def __init__(self, nums):
#         """
#         initialize your data structure here.
#         :type nums: List[int]
#         """
#         self.accu = [0]
#         for num in nums:
#             self.accu.append(self.accu[-1] + num),

#     def sumRange(self, i, j):
#         """
#         sum of elements nums[i..j], inclusive.
#         :type i: int
#         :type j: int
#         :rtype: int
#         """
#         return self.accu[j + 1] - self.accu[i]