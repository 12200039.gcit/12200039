def extendGCD(a, b):
    if b == 0:
        return [1, 0]
    result = extendGCD(b, a%b)
    
    x_dash = result[0]
    y_dash = result[1]

    x = y_dash
    y = x_dash - a//b*y_dash
 
    return (x,y)

def probOne(a,b):
    x, y = extendGCD(a,b)
    d = x*a + y*b
    print([x,y,d])
    
A = int(input('A '))
B = int(input('B '))

probOne(A,B)